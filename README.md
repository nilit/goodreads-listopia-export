Allows to export a goodreads books from a list, eg [best history books](https://www.goodreads.com/list/show/1362.Best_History_Books_nonfiction_) to a CSV file.

The book data includes:
- title
- author name
- author url
- rating from 1 to 5
- voters count
- book url

This script does't use goodreads API because it doesn't allow to export the list.

Usage:
- `git clone https://gitlab.com/nilit/goodreads-listopia-export.git`
- `cd goodreads`
- create a virtualenv with python 3.7+ and activate it
- `pip install -r requirements.txt`
- `python main.py --list-url=https://www.goodreads.com/list/show/1362.Best_History_Books_nonfiction_ --filename-for-export=books.csv --is-append-to-csv=true`

Not optimized, for a large list can require 8-9 GB of available RAM.

The results CSV file then can be imported into a google spreadsheet, where you can sort and filter it in any way you want.
