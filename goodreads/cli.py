from typing import List
from typing import Set

import click
from bs4 import BeautifulSoup

from goodreads.exporter import export_books_to_csv
from goodreads.models import Book
from goodreads.parser import finilize_book_elem_list
from goodreads.parser import get_first_page_content
from goodreads.parser import get_page_last
from goodreads.parser import init_book_bs_list
from goodreads.parser import parse_books


@click.command()
@click.option('--list-url', type=str, help='full https url to the first page of the list')
@click.option('--filename-for-export', type=str, default='book.csv')
@click.option('--is-append-to-csv', type=bool, default=True, help='when False the csv will be overridden')
def main(list_url: str, filename_for_export: str, is_append_to_csv: bool):
    page_first_content = get_first_page_content(list_url)
    
    book_elems_list: List[BeautifulSoup] = init_book_bs_list(page_first_content=page_first_content)
    page_last: int = get_page_last(page_first_content)
    
    print(f'loading {page_last} pages...')
    
    book_elems_list = finilize_book_elem_list(
        book_elems_list=book_elems_list,
        list_url=list_url,
        page_last=page_last,
    )
    
    books: Set[Book] = parse_books(book_elems_list)

    print(f'exporting...')
    
    export_books_to_csv(books, filename=filename_for_export, is_append_to_csv=is_append_to_csv)
